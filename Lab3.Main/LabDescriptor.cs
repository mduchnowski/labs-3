﻿using System;
using Lab3.Contract;
using Lab3.Implementation;


namespace Lab3
{
    public struct LabDescriptor
    {
        #region definitions

        public delegate object GetInstance(object component);
        
        #endregion

        #region P1

        public static Type I1 = typeof(ISterownik);
        public static Type I2 = typeof(IZasilacz);
        public static Type I3 = typeof(IWinda);

        public static Type Component = typeof(Winda);

        public static GetInstance GetInstanceOfI1 = x => (Winda)x as IZasilacz;
        public static GetInstance GetInstanceOfI2 = x => (Winda)x as ISterownik;
        public static GetInstance GetInstanceOfI3 = x => (Winda)x as IWinda;
        
        #endregion

        #region P2

        public static Type Mixin = typeof(Pasazer);
        public static Type MixinFor = typeof(IWinda);

        #endregion
    }
}
